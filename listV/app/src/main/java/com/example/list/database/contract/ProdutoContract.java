package com.example.list.database.contract;

        import com.example.list.database.entity.ProdutoEntity;
        import com.example.list.modelo.Produto;

public class ProdutoContract {

    private ProdutoContract(){}

    public static final String criarTabela(){
        return ("CREATE TABLE " + ProdutoEntity.TABLE_NAME) + " (" +
                ProdutoEntity._ID + " INTEGER PRIMARY KEY," +
                ProdutoEntity.COLUMN_NAME_NOME + " TEXT," +
                ProdutoEntity.COLUMN_NAME_LOCAL + " REAL," +
                ProdutoEntity.COLUMN_NAME_DATA + " TEXT)";
    }

    public static final String removerTabela(){
        return "DROP TABLE IF EXISTS " + ProdutoEntity.TABLE_NAME;
    }
}
