package com.example.list;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.list.modelo.Evento;
import com.example.list.modelo.Produto;

public class cadastro extends AppCompatActivity {

    private int id = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        setTitle("Cadastro evento");
        carregarEvento();
    }

    private void carregarEvento(){
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null && intent.getExtras().get("eventoEdicao") != null){
            Evento evento = (Evento) intent.getExtras().get("eventoEdicao");
            EditText etnome = findViewById(R.id.ET_nome);
            EditText etlocal = findViewById(R.id.ET_valor);
            EditText etdata = findViewById(R.id.ET_data);
            etnome.setText(evento.getNomeEvento());
            etlocal.setText(evento.getLocalEvento());
            etdata.setText(evento.getDataEvento());
            id = evento.getId();
        }
    }

    public void onClickVoltar(View v){
        finish();
    }

    public void onClicSalvar(View v){
        EditText etnome = findViewById(R.id.ET_nome);
        EditText etlocal = findViewById(R.id.ET_valor);
        EditText etdata = findViewById(R.id.ET_data);

        String nome = etnome.getText().toString();
        String data = String.valueOf(etdata.getText());
        String local = etlocal.getText().toString();

        Evento evento = new Evento(id, nome, local, data);
        ProdutoDAO produtoDAO = new ProdutoDAO(getBaseContext());
        boolean salvou = produtoDAO.salvar(evento);
        if (salvou){
            finish();
        }else{
            Toast.makeText(cadastro.this, "erro ao salvar", Toast.LENGTH_LONG);
        }
        }
    }
