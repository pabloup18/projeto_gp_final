package com.example.list;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.example.list.database.DBGateway;
import com.example.list.database.entity.ProdutoEntity;
import com.example.list.modelo.Evento;

import java.util.ArrayList;
import java.util.List;

public class ProdutoDAO {

    private final String SQL_LISTAR_TODOS = "SELECT * FROM " + ProdutoEntity.TABLE_NAME;
    private DBGateway dbGateway;

    public ProdutoDAO(Context context){
        dbGateway = DBGateway.getInstance(context);
    }

    public boolean salvar(Evento evento){
        ContentValues contentValues = new ContentValues();
        contentValues.put(ProdutoEntity.COLUMN_NAME_NOME, evento.getNomeEvento());
        contentValues.put(ProdutoEntity.COLUMN_NAME_LOCAL, evento.getLocalEvento());
        contentValues.put(ProdutoEntity.COLUMN_NAME_DATA, evento.getDataEvento());
        if (evento.getId() > 0 ){
            return dbGateway.getDatabase().update(ProdutoEntity.TABLE_NAME,
                    contentValues,
                    ProdutoEntity._ID + "=?",
                    new String[]{String.valueOf(evento.getId())}) > 0;
        }
        return dbGateway.getDatabase().insert(ProdutoEntity.TABLE_NAME, null, contentValues) > 0;
    }

     public List<Evento> listar(){
        List<Evento> eventos = new ArrayList<Evento>();
         Cursor cursor = dbGateway.getDatabase().rawQuery(SQL_LISTAR_TODOS, null);
         while (cursor.moveToNext()){
             int id = cursor.getInt(cursor.getColumnIndex(ProdutoEntity._ID));
             String nome = cursor.getString(cursor.getColumnIndex(ProdutoEntity.COLUMN_NAME_NOME));
             String local = cursor.getString(cursor.getColumnIndex(ProdutoEntity.COLUMN_NAME_LOCAL));
             String data = cursor.getString(cursor.getColumnIndex(ProdutoEntity.COLUMN_NAME_DATA));

             eventos.add(new Evento(id, nome, local, data));
         }
         cursor.close();
         return eventos;
     }
     public int ExcluirProduto(Evento evento){
         //ContentValues contentValues = new ContentValues();
         //contentValues.put(ProdutoEntity.COLUMN_NAME_NOME, produto.getNome());
         //contentValues.put(ProdutoEntity.COLUMN_NAME_VALOR, produto.getValor());
         int result = dbGateway.getDatabase().delete(ProdutoEntity.TABLE_NAME, ProdutoEntity._ID + "=?", new String[]{String.valueOf(evento.getId())});
         return result;
     }
}
